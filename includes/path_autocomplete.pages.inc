<?php

/**
 * Page callback for node/pathautocomplete.
 */
function path_autocomplete_callback($string = '') {
  $matches = array();

  $string = implode('/', func_get_args());
  if ($string) {
    $max = variable_get('path_autocomplete_max', 10);

    // Nodes.
    $result = db_select('node', 'n')
      ->fields('n')
      ->condition('title', '%' . db_like($string) . '%', 'LIKE')
      ->range(0, $max)
      ->execute();
    $nodes = array();
    foreach ($result as $node) {
      $nodes['node/' . $node->nid] = $node;
      $matches['node/' . $node->nid] = check_plain($node->title);
    }

    // Views.
    $string_lower = drupal_strtolower($string);
    $displays = _path_autocomplete_compile_views($views);
    foreach ($displays as $path => $label) {
      if (strpos(drupal_strtolower($label), $string_lower) !== FALSE) {
        $matches[$path] = $label;
      }
    }

    // Sort combined Nodes and Views.
    natcasesort($matches);

    // Let custom module handle sorting, labeling, etc.
    $context = compact('string', 'max', 'nodes', 'views');
    drupal_alter('path_autocomplete_paths', $matches, $context);

    // Truncate.
    if (count($matches) > $max) {
      $matches = array_slice($matches, 0, $max);
    }
  }

  drupal_json_output($matches);
}
