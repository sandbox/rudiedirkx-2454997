<?php

/**
 * Page/form callback for admin/config/content/pathautocomplete.
 */
function path_autocomplete_admin_form($form, &$form_state) {
  $views = module_exists('views') ? views_get_all_views() : array();

  $form['views'] = array(
    '#type' => 'fieldset',
    '#title' => t('Views'),
  );

  $displays = array();
  foreach ($views as $view_name => $view) {
    if (!empty($view->disabled)) continue;

    foreach ($view->display as $display_id => $display) {
      if (!empty($display->display_options['path'])) {
        $admin = strpos($display->display_options['path'], 'admin/') === 0;
        $label = _path_autocomplete_views_label($view, $display);
        if ($admin) {
          $label .= ' <em>(admin)</em>';
        }
        $displays[$view_name . ':' . $display_id] = $label;
      }
    }
  }
  natcasesort($displays);

  $form['views']['path_autocomplete_views_selection_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Selection mode'),
    '#options' => array(
      'excluding' => t('All but those selected'),
      'including' => t('Only those selected'),
    ),
    '#default_value' => variable_get('path_autocomplete_views_selection_mode', PATH_AUTOCOMPLETE_DEFAULT_VIEWS_SELECTION_MODE),
    '#required' => TRUE,
  );

  $form['views']['path_autocomplete_views_selection'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Selection'),
    '#options' => $displays,
    '#default_value' => variable_get('path_autocomplete_views_selection', array()),
  );

  $form['views']['path_autocomplete_views_exclude_admin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Exclude views in the <code>/admin/</code> URI space'),
    '#default_value' => variable_get('path_autocomplete_views_exclude_admin', PATH_AUTOCOMPLETE_DEFAULT_VIEWS_EXCLUDE_ADMIN),
    '#description' => t("With this enabled, you don't have to check/uncheck all the <em>(admin)</em> views above."),
  );

  $form['views']['path_autocomplete_views_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Views search label'),
    '#default_value' => variable_get('path_autocomplete_views_label', PATH_AUTOCOMPLETE_DEFAULT_VIEWS_LABEL),
    '#description' => t('This label will be used to search in and display to the searcher. Use tokens: <code>VIEW_TITLE</code>, <code>VIEW_DESCRIPTION</code>, <code>DISPLAY_TITLE</code>.'),
    '#required' => TRUE,
  );

  $form['#submit'][] = 'path_autocomplete_admin_form_submit';
  return system_settings_form($form);
}

/**
 * Submit handler for path_autocomplete_admin_form().
 */
function path_autocomplete_admin_form_submit($form, &$form_state) {
  $form_state['values']['path_autocomplete_views_selection'] = array_filter($form_state['values']['path_autocomplete_views_selection']);
}
