(function($) {

  Drupal.behaviors.pathAutocomplete = {
    attach: function(context, settings) {

      $('input[data-path-ac-title] + input.autocomplete', context).once('pathAutocomplete', function () {
        var $input = $('#' + this.id.substr(0, this.id.length - 13));
        $input.bind('autocompleteSelect', function(e, li) {
          var sel = this.getAttribute('data-path-ac-title');
          var $title = $(sel);
          if ($title.val() === '') {
            $title.val(li.textContent.trim());
          }
        });
      });

    }
  };

})(jQuery);
