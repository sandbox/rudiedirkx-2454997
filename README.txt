Makes path/link elements autocompletable to Nodes:

* Link field
* Menu item
* Path/URL alias
* Redirect

These modules do **almost** the same, but too much or too little:

* https://www.drupal.org/project/mpac
* https://www.drupal.org/project/advanced_link
* https://www.drupal.org/sandbox/jlyon/2168455
