<?php

/**
 * Implements hook_path_autocomplete_paths_alter().
 *
 * $context contains 'string', 'max' and 'nodes' (keyed by path).
 */
function hook_path_autocomplete_paths_alter(&$paths, $context) {
  $languages = language_list();
  $node_types = node_type_get_types();

  $regex = '#(' . preg_quote(check_plain($context['string']), '#') . ')#i';

  foreach ($paths as $path => &$title) {
    $title = preg_replace($regex, '<b style="font-size: 30px">$1</b>', $title);

    // Nodes get their type and language appended.
    $node = @$context['nodes'][$path];
    if ($node) {
      $node_type = $node_types[$node->type]->name;
      $language = @$languages[$node->language];
      $language = $language ? $language->name : '';

      $title = trim($title . ' - ' . t($node_type) . ' - ' . t($language), '- ');
    }

    // Views are pink.
    if (isset($context['views'][$path])) {
      $title = '<span style="color: pink">' . $title . '</span>';
    }
  }
}
